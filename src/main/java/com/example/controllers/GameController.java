package com.example.controllers;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import com.example.models.Game;
import com.example.views.GameView;

public class GameController implements ActionListener{

    Game model;
    GameView view;

    public GameController(Game game, GameView gameView){
        model= game;
        view= gameView;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton button= (JButton) e.getSource();
        switch(button.getText()){
            case "Check":
                playerCheck();
                break;
            case "Card":
                playerDrawCard();
                break;
            default:
                playerCancel();
                break;
        }
    }

    private void playerDrawCard(){
        model.getPlayer().addCard(model.getDeck().draw(false));
        view.getPlayerView().displayHands();
        if(model.valueOfHand(model.getPlayer())>21){
            model.getBanque().turnAllCard();
            view.getBanqueView().displayHands();
            view.getButtonCard().setEnabled(false);
            view.getButtonCheck().setEnabled(false);
            view.getResultLabel().setText("YOU LOSE!!!");
        }     
    }

    private void playerCheck(){
        model.getBanque().turnAllCard();
        view.getButtonCard().setEnabled(false);
        view.getButtonCheck().setEnabled(false);
        view.getBanqueView().displayHands();
        view.getPlayerView().displayHands();
        if(model.valueOfHand(model.getBanque())>21){
            view.getResultLabel().setText("YOU WIN!!!");
        }else if(model.valueOfHand(model.getPlayer())>model.valueOfHand(model.getBanque())){
            view.getResultLabel().setText("YOU WIN!!!");
        }else{
            view.getResultLabel().setText("YOU LOSE!!!");
        }
    }

    private void playerCancel(){
        model.initGame();
        view.getButtonCard().setEnabled(true);
        view.getButtonCheck().setEnabled(true);
        view.getResultLabel().setText("");
        view.getBanqueView().displayHands();
        view.getPlayerView().displayHands();
    }
}
