package com.example.models;

import java.util.ArrayList;

public class Player {
    
    private ArrayList<Card> hand;

    public Player(){
        hand= new ArrayList<Card>();
    }
    
    public void addCard(Card c){
        hand.add(c);
    }

    public void turnAllCard(){
        for(Card c : hand){
            c.setIsTurn(false);
        }
    }


    public ArrayList<Card> getHand() {
        return this.hand;
    }

    public void setHand(ArrayList<Card> hand) {
        this.hand = hand;
    }

    @Override
    public String toString() {
        String text="";
        for(Card c : hand){
            text += c+"\n";
        }
        return text;
    }
}
