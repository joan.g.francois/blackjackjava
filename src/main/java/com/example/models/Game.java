package com.example.models;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Game {
    private Player player, banque;
    private Deck deck;
    private static int ANSWER_CHECK=1;
    private static int ANSWER_CARD=2;
    private static int ANSWER_CANCEL=3;

    public Game(){
        player = new Player();
        banque = new Player();
        initGame();
    }

    public void initGame(){
        deck   = new Deck();
        player.setHand(new ArrayList<Card>());
        banque.setHand(new ArrayList<Card>());
        for(int i=0; i<2;i++){
            player.addCard(deck.draw(false));
            banque.addCard(deck.draw(i==0? true: false));
        }
        do{
            banque.addCard(deck.draw(false));
        }while(valueOfHand(banque)<17);
    }

    public Player getPlayer() {
        return this.player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Player getBanque() {
        return this.banque;
    }

    public void setBanque(Player banque) {
        this.banque = banque;
    }

    public Deck getDeck() {
        return this.deck;
    }

    public void setDeck(Deck deck) {
        this.deck = deck;
    }

    public void play(){
        int command;
        do{
            displayHands(false);
            command= anwserPlayer();
            if(command==ANSWER_CARD){
                player.addCard(deck.draw(false));
            }
            if(valueOfHand(player)>21){
                displayHands(true);
                System.out.println("You Lose!!!");
                command= ANSWER_CANCEL;
            }else if(valueOfHand(banque)>21){
                displayHands(true);
                System.out.println("You Win!!!");
                command= ANSWER_CANCEL;
            }else if(command==ANSWER_CHECK){
                displayHands(true);
                if(valueOfHand(player)<=valueOfHand(banque)){
                    System.out.println("You Lose!!!");
                }else{
                    System.out.println("You Win!!!");
                }
                command= ANSWER_CANCEL;
            }
        }while(command!=ANSWER_CANCEL);
    }

    public int anwserPlayer(){
        Set<String> set = new HashSet<String>();
        set.add("check");
        set.add("card");
        set.add("cancel");
        System.out.print("tape [check/card/cancel] :");
        String command = System.console().readLine();
        System.out.println(command);
        if(!set.contains(command)){
            System.out.print("Choix non valable!!!");
            return anwserPlayer();
        }
        switch(command){
            case "check":
                return ANSWER_CHECK;
            case "card":
                return ANSWER_CARD;
            default:
                return ANSWER_CANCEL;
        }
    }

    public void displayHands(boolean showBanque){
        if(showBanque){
            banque.turnAllCard();
        }
        System.out.println( "Banque:" + (showBanque? valueOfHand(banque): "") );
        System.out.print(banque);
        System.out.println( "Joueur: "+ valueOfHand(player));
        System.out.print(player);
    }

    public int valueOfHand(Player player){
        int cpt=0;
        int nbrAs=0;
        for(Card c: player.getHand()){
            if(c.getValue()==1){
                nbrAs++;
            }else if(c.getValue()<10){
                cpt+= c.getValue();
            }else{
                cpt+= 10;
            }
        }
        for(int i=nbrAs; i>0;i--){
            if(cpt+11<=21){
                cpt+=11;
            }else{
                cpt++;
            }
        }
        return cpt;
    }

    
}
