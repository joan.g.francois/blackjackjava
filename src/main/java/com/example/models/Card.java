package com.example.models;
import java.awt.*;

public class Card {

    private String color;
    private int value;
    private boolean isTurn;

  
    public Card(String color, int value) {
        this.color = color;
        this.value = value;
        this.isTurn= true;
    }

    public String getColor() {
        return this.color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getValue() {
        return this.value;
    }

    public void setValue(int value) {
        this.value = value;
    }


    public boolean isIsTurn() {
        return this.isTurn;
    }

    public boolean getIsTurn() {
        return this.isTurn;
    }

    public void setIsTurn(boolean isTurn) {
        this.isTurn = isTurn;
    }

    public String getValueSymbole(){
        switch(value){
            case 1:
                return "A";
            case 11:
                return "J";
            case 12:
                return "D";
            case 13:
                return "R";
            default:
                return value+"";
        }
    }

    public String getColorSymbole(){
        switch(color){
            case "Club":
                return "♣";
            case "Spade":
                return "♠";
            case "Diamond":
                return "♦";
            default:
                return "♥";
        }
    }

    public Color getCardColor(){
        if(isTurn){
            return Color.BLUE;
        }
        switch(color){
            case "Club":
            case "Spade":
                return Color.BLACK;
            case "Diamond":
            default:
                return Color.RED;
        }
    }

    @Override
    public String toString() {
        if(isTurn){
            return "{ *** - *** }";
        }
        return "{" +getValueSymbole()+" - "+ getColor() +"}";
    }

}
