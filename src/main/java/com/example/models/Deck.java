package com.example.models;

import java.util.ArrayList;
import java.util.Collections;

public class Deck {
    
    private ArrayList<Card> cards;

    private static String[] colors= {"Club", "Spade", "Diamond", "Heart"};

    public Deck(){
        cards= new ArrayList<Card>();
        for(String color: colors){
            for(int n=1;n<=13;n++){
                Card c= new Card(color, n);
                cards.add(c);
            }
        }
        Collections.shuffle(cards);
    }

    public Card draw(boolean turn){
        Card c=  cards.get(0);
        c.setIsTurn(turn);
        cards.remove(0);
        return c;
    }

}
