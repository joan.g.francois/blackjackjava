package com.example;
import com.example.models.Card;
import com.example.models.Deck;
import com.example.models.Game;
import com.example.views.GameView;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        GameView.getInstance();
    }  
}
