package com.example.views;

import javax.swing.*;
import java.awt.*;

import com.example.controllers.GameController;
import com.example.models.Game;

import java.nio.file.Path;
import java.nio.file.Paths;

public class GameView extends JFrame{
    private JPanel mainPanel, buttonPanel;
    private PlayerView banqueView, playerView;
    private Game model;
    private GameController controller;
    private JButton buttonCheck, buttonCard, buttonCancel;
    private JLabel resultLabel;

    private static GameView instance;

    public static GameView getInstance(){
        if(instance==null){
            instance= new GameView();
        }
        return instance;
    }

    
    private GameView() {
        super("BlackJack");
        model = new Game();
        controller= new GameController(model, this);
        Path path = Paths.get("images/background.jpg");
        final Image backgroundImage = Toolkit.getDefaultToolkit().getImage(path.toAbsolutePath().toString());
    
        // Create and set up a frame window
        //JFrame.setDefaultLookAndFeelDecorated(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        // Define the panel to hold the buttons
        mainPanel = new JPanel(){
            @Override
            public void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(backgroundImage, 0, 0, null);
                repaint();
            }
            @Override
            public Dimension getPreferredSize() {
                return new Dimension(612, 520);
            }
        };
        banqueView= new PlayerView("Banque", model.getBanque());
        playerView= new PlayerView("Player", model.getPlayer());
        
        buttonPanel= new JPanel();
        buttonPanel.setOpaque(false);
        buttonPanel.setLayout(new FlowLayout());

        buttonCheck = new JButton("Check");
        buttonCheck.addActionListener(controller);
        buttonCard = new JButton("Card");
        buttonCard.addActionListener(controller);
        buttonCancel = new JButton("Reset");     
        buttonCancel.addActionListener(controller);
        
        buttonPanel.add(buttonCheck);
        buttonPanel.add(buttonCard);
        buttonPanel.add(buttonCancel);

        mainPanel.setLayout(new GridLayout(4,1));
        mainPanel.add(banqueView);
        mainPanel.add(playerView);
        mainPanel.add(buttonPanel);
        resultLabel= new JLabel("");
        JPanel resultPanel= new JPanel();
        resultPanel.add(resultLabel);

        mainPanel.add(resultPanel);
        // Set the window to be visible as the default to be false
        add(mainPanel);
        pack();
        setVisible(true);     
    }

    public PlayerView getBanqueView() {
        return this.banqueView;
    }

    public PlayerView getPlayerView() {
        return this.playerView;
    }

    public JButton getButtonCheck() {
        return this.buttonCheck;
    }

    public JButton getButtonCard() {
        return this.buttonCard;
    }

    public JButton getButtonCancel() {
        return this.buttonCancel;
    }

    public JLabel getResultLabel() {
        return this.resultLabel;
    }
}
