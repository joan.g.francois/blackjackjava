package com.example.views;

import javax.swing.JPanel;

import com.example.models.Card;
import com.example.models.Player;

import javax.swing.*;
import java.awt.*;

public class PlayerView extends JPanel{
    
    private Player model;
    private String name;
    public PlayerView(String name, Player player){
        super();
        setOpaque(false);
        setLayout(new FlowLayout());
        this.name= name;
        model= player;
        displayHands();
    }

    public void displayHands(){
        this.removeAll();
        JLabel label= new JLabel(name+":");
        label.setForeground(Color.WHITE);
        add(label);

        for(Card c: model.getHand()){
            add(new CardView(c));
        }
        validate();
        repaint();
    }
}
