package com.example.views;

import javax.swing.*;
import java.awt.*;
import com.example.models.Card;


public class CardView extends JPanel {

    private static int width= 80;
    private static int height= 120;
    private Card model;

    public CardView(Card card){
        this.model= card;
    }

    @Override
    public void paintComponent(Graphics g) {
        Graphics2D graphic2d = (Graphics2D) g;
        graphic2d.setColor(model.getCardColor());
        graphic2d.fillRect(0, 0, width, height);
        graphic2d.setColor(Color.white);
        graphic2d.fillRect(5, 5, width-10, height-10);
        graphic2d.setColor(model.getCardColor());
        if(model.getIsTurn()){
            graphic2d.drawString( "Card", width/2-11, height/2+7);
        }else{
            graphic2d.drawString( model.getColorSymbole(), 10, 20);
            graphic2d.drawString( model.getColorSymbole(), width-20, 20);
            graphic2d.drawString( model.getValueSymbole(), width/2-5, height/2+7);
            graphic2d.drawString( model.getColorSymbole(), 10, height-15);
            graphic2d.drawString( model.getColorSymbole(), width-20, height-15);
        }
        repaint();
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(width, height);
    }
    
}
