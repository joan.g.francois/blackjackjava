# BlackJACK JAVA

Voici un petit projet JAVA où vous pouvez tester les classes modèles et le jeux en console avant d'implémenter l'interface graphique en SWING.

# Diagramme de Classe

![Diagramme de Classe](images/DiagrammeClasseBlackJack.jpg "Diagramme de Classe")

# Screen

![Screen](images/ScreenBJ.png "Screen")

